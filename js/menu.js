jQuery(document).ready(function($) {

  // if ($(window).width() < 768) {
  //   $("#block-search-form .container-inline").hide();
  // }
  // else {
  //   $("#block-search-form .container-inline").show();
  // }

  // $(window).on('resize', function() {
  //     if ($(window).width() < 768) {
  //       $("#block-search-form .container-inline").hide();
  //     }
  //     else {
  //       $("#block-search-form .container-inline").show();
  //     }
  // });

  $("#search-click").click(function() {
    $("#block-search-form .container-inline").slideToggle("fast");
  });

  $("#section-mobilenav").hide();
  $(".mobile-anchor").click(function() {
    $("#section-mobilenav").slideToggle( 0 );
  });

  $(".block-bean .par").hide();
  $(".block-bean h3.header").click(function() {
    $(this).css("border-radius", "5px 5px 0 0");
    $(this).next().slideToggle( 250 );
  });
});