<?php

/**
 * @file
 * This file provides theme override functions for the HFCC theme.
 */

/**
 * Implements hook_page_alter().
 */
function hfcc_page_alter(&$page) {
  if (!empty($page['content']['system_main']['nodes'])) {
    $nodes = &$page['content']['system_main']['nodes'];
    $nid = key($nodes);
    if (!empty($nodes[$nid]['field_leftnav'])) {
      $page['sidebar_first']['field_leftnav'] = $nodes[$nid]['field_leftnav'];
      $page['sidebar_first']['field_leftnav']['#weight'] = '-99';
      $page['sidebar_first']['#sorted'] = FALSE;
      if (empty($page['sidebar_first']['#region'])) {
        $page['sidebar_first']['#region'] = 'sidebar_first';
        $page['sidebar_first']['#theme_wrappers'] = array('region');
      }
      unset($nodes[$nid]['field_leftnav']);
    }
  }
}

/**
 * Implements hook_template_preprocess_page().
 *
 * Preprocess variables for page.tpl.php.
 */
function hfcc_preprocess_page(&$variables) {
  if (@$variables['node']->type == 'tuition_compare') {
    $title = t('Compare HFC Tuition to @title', array('@title' => $variables['node']->title));
    drupal_set_title($title, PASS_THROUGH);
  }
}
